#ifndef SINGLETON_H
#define SINGLETON_H

#include <memory>
#include <mutex>

#define SINGLETON(type) Utils::Singleton<type>::instance().get()

namespace Utils {

template<class T>
class Singleton {
public:
    using pointer_t = std::unique_ptr<T>;

    static const pointer_t& instance()
    {
        if (m_instance == nullptr) {
            std::lock_guard<std::mutex> lock(m_mutex);
            if (m_instance == nullptr) {
                m_instance = std::make_unique<T>();
            }
        }
        return m_instance;
    }

    static void clean()
    {
        std::lock_guard<std::mutex> lock(m_mutex);

        m_instance.clear();
    }

private:
    static pointer_t m_instance;
    static std::mutex m_mutex;
};

template<class T>
typename Singleton<T>::pointer_t Singleton<T>::m_instance;

template<class T>
typename std::mutex Singleton<T>::m_mutex;

} // namespace Utils

#endif // SINGLETON_H
