#ifndef PROPERTY_H
#define PROPERTY_H

#define PROPERTY(type, name)                                                   \
    PROPERTY_DECL(type, name)                                                  \
    PROPERTY_SET(type, name)                                                   \
    PROPERTY_GET(type, name)

#define PROPERTY_CUSTOM_SET(type, name)                                        \
    PROPERTY_DECL(type, name)                                                  \
    PROPERTY_GET(type, name)                                                   \
public:                                                                        \
    virtual void set_##name(type a);

#define PROPERTY_DECL(type, name)                                              \
protected:                                                                     \
    type m_##name;

#define PROPERTY_GET(type, name)                                               \
public:                                                                        \
    virtual type& get_##name() { return m_##name; }                            \
    virtual type  get_##name() const { return m_##name; }

#define PROPERTY_SET(type, name)                                               \
public:                                                                        \
    virtual void set_##name(type a)                                            \
    {                                                                          \
        if (m_##name == a)                                                     \
            return;                                                            \
        m_##name = a;                                                          \
    }

#endif
