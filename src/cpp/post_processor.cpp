#include "post_processor.hpp"

#include <iostream>

PostProcessor::PostProcessor(Shader       shader,
                             unsigned int width,
                             unsigned int height)
    : m_PostProcessingShader(shader)
    , m_Texture()
    , m_Width(width)
    , m_Height(height)
    , m_Confuse(false)
    , m_Chaos(false)
    , m_Shake(false)
{
    glGenFramebuffers(1, &this->m_MSFBO);
    glGenFramebuffers(1, &this->m_FBO);
    glGenRenderbuffers(1, &this->m_RBO);

    glBindFramebuffer(GL_FRAMEBUFFER, this->m_MSFBO);
    glBindRenderbuffer(GL_RENDERBUFFER, this->m_RBO);
    glRenderbufferStorageMultisample(GL_RENDERBUFFER, 4, GL_RGB, width, height);
    glFramebufferRenderbuffer(
        GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_RENDERBUFFER, this->m_RBO);
    if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
    {
        std::cout << "ERROR::POSTPROCESSOR: Failed to initialize MSFBO"
                  << std::endl;
    }
    glBindFramebuffer(GL_FRAMEBUFFER, this->m_FBO);
    this->m_Texture.Generate(width, height, NULL);
    glFramebufferTexture2D(
        GL_FRAMEBUFFER,
        GL_COLOR_ATTACHMENT0,
        GL_TEXTURE_2D,
        this->m_Texture.ID,
        0); // attach texture to framebuffer as its color attachment
    if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
        std::cout << "ERROR::POSTPROCESSOR: Failed to initialize FBO"
                  << std::endl;

    glBindFramebuffer(GL_FRAMEBUFFER, 0);

    this->initRenderData();
    this->m_PostProcessingShader.SetInteger("scene", 0, true);
    float offset        = 1.0f / 300.0f;
    float offsets[9][2] = { { -offset, offset },  { 0.0f, offset },
                            { offset, offset },   { -offset, 0.0f },
                            { 0.0f, 0.0f },       { offset, 0.0f },
                            { -offset, -offset }, { 0.0f, -offset },
                            { offset, -offset } };
    glUniform2fv(
        glGetUniformLocation(this->m_PostProcessingShader.ID, "offsets"),
        9,
        (float*)offsets);
    int edge_kernel[9] = { -1, -1, -1, -1, 8, -1, -1, -1, -1 };
    glUniform1iv(
        glGetUniformLocation(this->m_PostProcessingShader.ID, "edge_kernel"),
        9,
        edge_kernel);
    float blur_kernel[9] = { 1.0f / 16.0f, 2.0f / 16.0f, 1.0f / 16.0f,
                             2.0f / 16.0f, 4.0f / 16.0f, 2.0f / 16.0f,
                             1.0f / 16.0f, 2.0f / 16.0f, 1.0f / 16.0f };
    glUniform1fv(
        glGetUniformLocation(this->m_PostProcessingShader.ID, "blur_kernel"),
        9,
        blur_kernel);
}

void PostProcessor::BeginRender()
{
    glBindFramebuffer(GL_FRAMEBUFFER, this->m_MSFBO);
    glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
    glClear(GL_COLOR_BUFFER_BIT);
}

void PostProcessor::EndRender()
{
    glBindFramebuffer(GL_READ_FRAMEBUFFER, this->m_MSFBO);
    glBindFramebuffer(GL_DRAW_FRAMEBUFFER, this->m_FBO);
    glBlitFramebuffer(0,
                      0,
                      this->m_Width,
                      m_Height,
                      0,
                      0,
                      this->m_Width,
                      this->m_Height,
                      GL_COLOR_BUFFER_BIT,
                      GL_NEAREST);
    glBindFramebuffer(GL_FRAMEBUFFER, 0);
}

void PostProcessor::Render(float time)
{
    this->m_PostProcessingShader.Use();
    this->m_PostProcessingShader.SetFloat("time", time);
    this->m_PostProcessingShader.SetInteger("confuse", this->m_Confuse);
    this->m_PostProcessingShader.SetInteger("chaos", this->m_Chaos);
    this->m_PostProcessingShader.SetInteger("shake", this->m_Shake);

    glActiveTexture(GL_TEXTURE0);
    this->m_Texture.Bind();
    glBindVertexArray(this->m_VAO);
    glDrawArrays(GL_TRIANGLES, 0, 6);
    glBindVertexArray(0);
}

void PostProcessor::initRenderData()
{
    unsigned int VBO;
    float        vertices[] = { -1.0f, -1.0f, 0.0f,  0.0f, 1.0f, 1.0f,
                         1.0f,  1.0f,  -1.0f, 1.0f, 0.0f, 1.0f,

                         -1.0f, -1.0f, 0.0f,  0.0f, 1.0f, -1.0f,
                         1.0f,  0.0f,  1.0f,  1.0f, 1.0f, 1.0f };
    glGenVertexArrays(1, &this->m_VAO);
    glGenBuffers(1, &VBO);

    glBindBuffer(GL_ARRAY_BUFFER, VBO);
    glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);

    glBindVertexArray(this->m_VAO);
    glEnableVertexAttribArray(0);
    glVertexAttribPointer(
        0, 4, GL_FLOAT, GL_FALSE, 4 * sizeof(float), (void*)0);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glBindVertexArray(0);
}
