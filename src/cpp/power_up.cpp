#include "power_up.hpp"

bool operator==(const PowerUp& pu1, const PowerUp& pu2)
{
    return (pu1.get_Activated() == pu2.get_Activated() &&
            pu1.get_Color() == pu2.get_Color() &&
            pu1.get_Destroyed() == pu2.get_Destroyed() &&
            pu1.get_Duration() == pu2.get_Duration() &&
            pu1.get_IsSolid() == pu2.get_IsSolid() &&
            pu1.get_Position() == pu2.get_Position() &&
            pu1.get_Rotation() == pu2.get_Rotation() &&
            pu1.get_Size() == pu2.get_Size() &&
            pu1.get_Sprite() == pu2.get_Sprite() &&
            pu1.get_Type() == pu2.get_Type() &&
            pu1.get_Velocity() == pu2.get_Velocity());
}
