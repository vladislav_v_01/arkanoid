#include "game.hpp"
#include "ball_object.hpp"
#include "gameobject.hpp"
#include "particle_generator.hpp"
#include "post_processor.hpp"
#include "resource_manager.h"
#include "sprite_renderer.hpp"
#include "text_renderer.hpp"

#include <iostream>
#include <sstream>

#include <QDir>
#include <QFileInfo>

SpriteRenderer*    Renderer;
GameObject*        Player;
BallObject*        Ball;
ParticleGenerator* Particles;
PostProcessor*     Effects;
TextRenderer*      Text;

float ShakeTime = 0.0f;

Game::Game(unsigned int width, unsigned int height)

    : m_KeysProcessed()
    , m_State(GameState::GAME_MENU)
    , m_Width(width)
    , m_Height(height)
    , m_Level()
    , m_Lives(3)
    , m_Keys()
    , m_Levels()

{
}

Game::~Game()
{
    delete Renderer;
    delete Player;
    delete Ball;
    delete Particles;
    delete Effects;
    delete Text;
}

void Game::Init()
{
    ResourceManager::LoadShader("../../src/shaders/sprite.vs",
                                "../../src/shaders/sprite.fs",
                                nullptr,
                                "sprite");
    ResourceManager::LoadShader("../../src/shaders/particle.vs",
                                "../../src/shaders/particle.fs",
                                nullptr,
                                "particle");
    ResourceManager::LoadShader("../../src/shaders/postprocessing.vs",
                                "../../src/shaders/postprocessing.fs",
                                nullptr,
                                "postprocessing");

    glm::mat4 projection = glm::ortho(0.0f,
                                      static_cast<float>(this->m_Width),
                                      static_cast<float>(this->m_Height),
                                      0.0f,
                                      -1.0f,
                                      1.0f);

    ResourceManager::GetShader("sprite").Use().SetInteger("image", 0);
    ResourceManager::GetShader("sprite").SetMatrix4("projection", projection);
    ResourceManager::GetShader("particle").Use().SetInteger("sprite", 0);
    ResourceManager::GetShader("particle").SetMatrix4("projection", projection);

    ResourceManager::LoadTexture(
        "../../src/textures/awesomeface.png", true, "face");
    ResourceManager::LoadTexture(
        "../../src/textures/background.jpg", false, "background");
    ResourceManager::LoadTexture(
        "../../src/textures/block.png", false, "block");
    ResourceManager::LoadTexture(
        "../../src/textures/block_solid.png", false, "block_solid");
    ResourceManager::LoadTexture(
        "../../src/textures/paddle.png", true, "paddle");
    ResourceManager::LoadTexture(
        "../../src/textures/particle.png", true, "particle");
    ResourceManager::LoadTexture(
        "../../src/textures/powerup_speed.png", true, "powerup_speed");
    ResourceManager::LoadTexture(
        "../../src/textures/powerup_sticky.png", true, "powerup_sticky");
    ResourceManager::LoadTexture(
        "../../src/textures/powerup_increase.png", true, "powerup_increase");
    ResourceManager::LoadTexture(
        "../../src/textures/powerup_confuse.png", true, "powerup_confuse");
    ResourceManager::LoadTexture(
        "../../src/textures/powerup_chaos.png", true, "powerup_chaos");
    ResourceManager::LoadTexture("../../src/textures/powerup_passthrough.png",
                                 true,
                                 "powerup_passthrough");

    Renderer  = new SpriteRenderer(ResourceManager::GetShader("sprite"));
    Particles = new ParticleGenerator(ResourceManager::GetShader("particle"),
                                      ResourceManager::GetTexture("particle"),
                                      500);
    Effects   = new PostProcessor(ResourceManager::GetShader("postprocessing"),
                                this->m_Width,
                                this->m_Height);

    GameLevel one;
    one.Load("../../src/levels/one.lvl", m_Width, m_Height / 2.0);
    GameLevel two;
    two.Load("../../src/levels/two.lvl", m_Width, m_Height / 2.0);
    GameLevel three;
    three.Load("../../src/levels/three.lvl", m_Width, m_Height / 2.0);
    GameLevel four;
    four.Load("../../src/levels/four.lvl", m_Width, m_Height / 2.0);
    m_Levels.push_back(one);
    m_Levels.push_back(two);
    m_Levels.push_back(three);
    m_Levels.push_back(four);
    m_Level = 0;

    glm::vec2 playerPos = glm::vec2(this->m_Width / 2.0f - PLAYER_SIZE.x / 2.0f,
                                    this->m_Height - PLAYER_SIZE.y);
    Player              = new GameObject(
        playerPos, PLAYER_SIZE, ResourceManager::GetTexture("paddle"));

    glm::vec2 ballPos =
        playerPos +
        glm::vec2(PLAYER_SIZE.x / 2.0f - BALL_RADIUS, -BALL_RADIUS * 2.0f);
    Ball = new BallObject(ballPos,
                          BALL_RADIUS,
                          INITIAL_BALL_VELOCITY,
                          ResourceManager::GetTexture("face"));
    Text = new TextRenderer(this->m_Width, this->m_Height);
    Text->Load("../../src/fonts/OCRAEXT.TTF", 24);
}

void Game::Update(float dt)
{
    Ball->Move(dt, m_Width);

    this->DoCollision();

    Particles->Update(dt, *Ball, 2, glm::vec2(Ball->get_Radius() / 2.0f));

    this->UpdatePowerUps(dt);

    if (ShakeTime > 0.0f)
    {
        ShakeTime -= dt;
        if (ShakeTime <= 0.0f)
        {
            Effects->set_Shake(false);
        }
    }

    if (Ball->get_Position().y >= this->m_Height)
    {
        --this->m_Lives;
        if (this->m_Lives == 0)
        {

            this->ResetLevel();
            this->m_State = GameState::GAME_MENU;
        }
        this->ResetPlayer();
    }

    if (this->m_State == GameState::GAME_ACTIVE &&
        this->m_Levels[this->m_Level].IsCompleted())
    {
        this->ResetLevel();
        this->ResetPlayer();
        Effects->set_Chaos(true);
        this->m_State = GameState::GAME_WIN;
    }
}

void Game::ProcessInput(float dt)
{
    if (this->m_State == GameState::GAME_ACTIVE)
    {
        float velocity = PLAYER_VELOCITY * dt;
        if (this->m_Keys[GLFW_KEY_A])
        {
            if (Player->get_Position().x >= 0.0f)
            {
                glm::vec2 newPosition(Player->get_Position().x - velocity,
                                      Player->get_Position().y);
                Player->set_Position(newPosition);

                if (Ball->get_Stuck())
                {
                    glm::vec2 newBallPosition(Ball->get_Position().x - velocity,
                                              Ball->get_Position().y);
                    Ball->set_Position(newBallPosition);
                }
            }
        }
        if (this->m_Keys[GLFW_KEY_D])
        {
            if (Player->get_Position().x <=
                this->m_Width - Player->get_Size().x)
            {
                glm::vec2 newPlayerPosition(Player->get_Position().x + velocity,
                                            Player->get_Position().y);
                Player->set_Position(newPlayerPosition);

                if (Ball->get_Stuck())
                {
                    glm::vec2 newBallPosition(Ball->get_Position().x + velocity,
                                              Ball->get_Position().y);
                    Ball->set_Position(newBallPosition);
                }
            }
        }
        if (m_Keys[GLFW_KEY_SPACE])
        {
            Ball->set_Stuck(false);
        }
    }
    if (this->m_State == GameState::GAME_MENU)
    {
        if (this->m_Keys[GLFW_KEY_ENTER] &&
            !this->m_KeysProcessed[GLFW_KEY_ENTER])
        {
            this->m_State                         = GameState::GAME_ACTIVE;
            this->m_KeysProcessed[GLFW_KEY_ENTER] = true;
        }
        if (this->m_Keys[GLFW_KEY_W] && !this->m_KeysProcessed[GLFW_KEY_W])
        {
            this->m_Level                     = (this->m_Level + 1) % 4;
            this->m_KeysProcessed[GLFW_KEY_W] = true;
        }
        if (this->m_Keys[GLFW_KEY_S] && !this->m_KeysProcessed[GLFW_KEY_S])
        {
            if (this->m_Level > 0)
                --this->m_Level;
            else
                this->m_Level = 3;
            this->m_KeysProcessed[GLFW_KEY_S] = true;
        }
    }
    if (this->m_State == GameState::GAME_WIN)
    {
        if (this->m_Keys[GLFW_KEY_ENTER])
        {
            this->m_KeysProcessed[GLFW_KEY_ENTER] = true;
            Effects->set_Chaos(false);
            this->m_State = GameState::GAME_MENU;
        }
    }
}

void Game::Render()
{
    if (this->m_State == GameState::GAME_ACTIVE ||
        this->m_State == GameState::GAME_MENU ||
        this->m_State == GameState::GAME_WIN)
    {
        Effects->BeginRender();
        {
            Renderer->DrawSprite(ResourceManager::GetTexture("background"),
                                 glm::vec2(0.0f, 0.0f),
                                 glm::vec2(m_Width, m_Height),
                                 0.0f);
            this->m_Levels[this->m_Level].Draw(*Renderer);

            Player->Draw(*Renderer);

            for (PowerUp& powerUp : this->m_PowerUps)
            {
                if (!powerUp.get_Destroyed())
                {
                    powerUp.Draw(*Renderer);
                }
            }

            Particles->Draw();

            Ball->Draw(*Renderer);
        }

        Effects->EndRender();

        Effects->Render(glfwGetTime());

        std::stringstream ss;
        ss << this->m_Lives;
        Text->RenderText("Lives: " + ss.str(), 5.0f, 5.0f, 1.0f);
    }
    if (this->m_State == GameState::GAME_MENU)
    {
        Text->RenderText("Press ENTER to start", 250.0f, m_Height / 2, 1.0f);
        Text->RenderText("Press W or S to select level",
                         245.0f,
                         m_Height / 2 + 20.0f,
                         0.75f);
    }
    if (this->m_State == GameState::GAME_WIN)
    {
        Text->RenderText("YOU WON!!!",
                         320.0,
                         m_Height / 2 - 20.0,
                         1.0,
                         glm::vec3(0.0, 1.0, 0.0));
        Text->RenderText("Press ENTER to retry or ESC to quit",
                         130,
                         m_Height / 2,
                         1.0,
                         glm::vec3(1.0, 1.0, 0.0));
    }
}

void ActivatePowerUp(PowerUp& powerUp)
{
    if (powerUp.get_Type() == "speed")
    {
        Ball->set_Velocity(glm::vec2(Ball->get_Velocity().x * 1.2,
                                     Ball->get_Velocity().y * 1.2));
    }
    else if (powerUp.get_Type() == "sticky")
    {
        Ball->set_Sticky(true);
        Player->set_Color(glm::vec3(1.0f, 0.5f, 1.0f));
    }
    else if (powerUp.get_Type() == "pass-through")
    {
        Ball->set_PassThrough(true);
        Ball->set_Color(glm::vec3(1.0f, 0.5f, 0.5f));
    }
    else if (powerUp.get_Type() == "pad-size-increase")
    {
        Player->set_Size(
            glm::vec2(Player->get_Size().x + 50, Player->get_Size().y));
    }
    else if (powerUp.get_Type() == "confuse")
    {
        if (!Effects->get_Chaos())
        {
            Effects->set_Confuse(true);
        }
    }
    else if (powerUp.get_Type() == "chaos")
    {
        if (!Effects->get_Confuse())
        {
            Effects->set_Chaos(true);
        }
    }
}

bool      CheckCollision(const GameObject& one, const GameObject& two);
Collision CheckCollosion(const BallObject& one, const GameObject& two);
Direction VectorDirection(glm::vec2 target);

void Game::DoCollision()
{
    for (GameObject& box : m_Levels[m_Level].get_Bricks())
    {
        if (!box.get_Destroyed())
        {
            Collision collision = CheckCollosion(*Ball, box);
            if (std::get<0>(collision))
            {
                if (!box.get_IsSolid())
                {
                    box.set_Destroyed(true);
                    this->SpawnPowerUps(box);
                }
                else
                {
                    ShakeTime = 0.05f;
                    Effects->set_Shake(true);
                }
                Direction dir         = std::get<1>(collision);
                glm::vec2 diff_vector = std::get<2>(collision);
                if (!(Ball->get_PassThrough() &&
                      !box.get_IsSolid())) // don't do collision resolution on
                                           // non-solid bricks if pass-through
                                           // is activated
                {
                    if (dir == Direction::LEFT || dir == Direction::RIGHT)
                    {
                        glm::vec2 newBallVelocity = glm::vec2(
                            -Ball->get_Velocity().x, Ball->get_Velocity().y);
                        Ball->set_Velocity(newBallVelocity);

                        float penetration =
                            Ball->get_Radius() - std::abs(diff_vector.x);

                        if (dir == Direction::LEFT)
                        {
                            // move right
                            glm::vec2 newBallPosition =
                                glm::vec2(Ball->get_Position().x + penetration,
                                          Ball->get_Position().y);
                            Ball->set_Position(newBallPosition);
                        }
                        else
                        {
                            // move left
                            glm::vec2 newBallPosition =
                                glm::vec2(Ball->get_Position().x - penetration,
                                          Ball->get_Position().y);
                            Ball->set_Position(newBallPosition);
                        }
                    }
                    else
                    {
                        glm::vec2 newBallVelocity = glm::vec2(
                            Ball->get_Velocity().x, -Ball->get_Velocity().y);
                        Ball->set_Velocity(newBallVelocity);

                        float penetration =
                            Ball->get_Radius() - std::abs(diff_vector.y);

                        if (dir == Direction::UP)
                        {
                            // move up
                            glm::vec2 newBallPosition =
                                glm::vec2(Ball->get_Position().x,
                                          Ball->get_Position().y - penetration);
                            Ball->set_Position(newBallPosition);
                        }
                        else
                        {
                            // move down
                            glm::vec2 newBallPosition =
                                glm::vec2(Ball->get_Position().x,
                                          Ball->get_Position().y + penetration);
                            Ball->set_Position(newBallPosition);
                        }
                    }
                }
            }
        }
    }

    for (PowerUp& powerUp : this->m_PowerUps)
    {
        if (!powerUp.get_Destroyed())
        {
            if (powerUp.get_Position().y >= this->get_Height())
            {
                powerUp.set_Destroyed(true);
            }
            if (CheckCollision(*Player, powerUp))
            {
                ActivatePowerUp(powerUp);
                powerUp.set_Destroyed(true);
                powerUp.set_Activated(true);
            }
        }
    }

    Collision result = CheckCollosion(*Ball, *Player);
    if (!Ball->get_Stuck() && std::get<0>(result))
    {
        float centerBoard =
            Player->get_Position().x + Player->get_Size().x / 2.0f;
        float distance =
            (Ball->get_Position().x + Ball->get_Radius()) - centerBoard;
        float percentage = distance / (Player->get_Size().x / 2.0f);

        float     strength    = 2.0f;
        glm::vec2 oldVelocity = Ball->get_Velocity();

        glm::vec2 newBallVelocity(0.0f, 0.0f);
        newBallVelocity.x = INITIAL_BALL_VELOCITY.x * percentage * strength;
        Ball->set_Velocity(glm::vec2(newBallVelocity.x, oldVelocity.y));
        Ball->set_Velocity(glm::normalize(Ball->get_Velocity()) *
                           glm::length(oldVelocity));
        newBallVelocity.y = -1.0 * std::abs(Ball->get_Velocity().y);
        Ball->set_Velocity(
            glm::vec2(Ball->get_Velocity().x, newBallVelocity.y));

        Ball->set_Stuck(Ball->get_Sticky());
    }
}

void Game::ResetLevel()
{
    if (this->m_Level == 0)
    {
        this->m_Levels[0].Load(
            "../../src/levels/one.lvl", this->m_Width, this->m_Height / 2);
    }
    else if (this->m_Level == 1)
    {
        this->m_Levels[1].Load(
            "../../src/levels/two.lvl", this->m_Width, this->m_Height / 2);
    }
    else if (this->m_Level == 2)
    {
        this->m_Levels[2].Load(
            "../../src/levels/three.lvl", this->m_Width, this->m_Height / 2);
    }
    else if (this->m_Level == 3)
    {
        this->m_Levels[3].Load(
            "../../src/levels/four.lvl", this->m_Width, this->m_Height / 2);
    }
    this->m_Lives = 3;
}

void Game::ResetPlayer()
{
    Player->set_Size(PLAYER_SIZE);
    Player->set_Position(glm::vec2(this->m_Width / 2.0f - PLAYER_SIZE.x / 2.0f,
                                   this->m_Height - PLAYER_SIZE.y));
    Ball->Reset(Player->get_Position() +
                    glm::vec2(PLAYER_SIZE.x / 2.0f - BALL_RADIUS,
                              -(BALL_RADIUS * 2.0f)),
                INITIAL_BALL_VELOCITY);

    Effects->set_Chaos(false);
    Effects->set_Confuse(false);
    Ball->set_PassThrough(false);
    Ball->set_Sticky(false);
    Player->set_Color(glm::vec3(1.0f));
    Ball->set_Color(glm::vec3(1.0f));
}

bool IsOtherPowerUpActive(std::vector<PowerUp>& powerUps, std::string type);

void Game::UpdatePowerUps(float dt)
{
    for (PowerUp& powerUp : this->m_PowerUps)
    {
        powerUp.set_Position(
            glm::vec2(powerUp.get_Position() + powerUp.get_Velocity() * dt));
        if (powerUp.get_Activated())
        {
            powerUp.set_Duration(powerUp.get_Duration() - dt);

            if (powerUp.get_Duration() <= 0.0f)
            {
                // remove powerup from list (will later be removed)
                powerUp.set_Activated(false);
                // deactivate effects
                if (powerUp.get_Type() == "sticky")
                {
                    if (!IsOtherPowerUpActive(this->m_PowerUps, "sticky"))
                    { // only reset if no other PowerUp of type sticky is active
                        Ball->set_Sticky(false);
                        Player->set_Color(glm::vec3(1.0f));
                    }
                }
                else if (powerUp.get_Type() == "pass-through")
                {
                    if (!IsOtherPowerUpActive(this->m_PowerUps, "pass-through"))
                    { // only reset if no other PowerUp of type pass-through is
                      // active
                        Ball->set_PassThrough(false);
                        Ball->set_Color(glm::vec3(1.0f));
                    }
                }
                else if (powerUp.get_Type() == "confuse")
                {
                    if (!IsOtherPowerUpActive(this->m_PowerUps, "confuse"))
                    { // only reset if no other PowerUp of type confuse is
                      // active
                        Effects->set_Confuse(false);
                    }
                }
                else if (powerUp.get_Type() == "chaos")
                {
                    if (!IsOtherPowerUpActive(this->m_PowerUps, "chaos"))
                    { // only reset if no other PowerUp of type chaos is active
                        Effects->set_Chaos(false);
                    }
                }
            }
        }
    }
    // Remove all PowerUps from vector that are destroyed AND !activated (thus
    // either off the map or finished) Note we use a lambda expression to remove
    // each PowerUp which is destroyed and not activated
    this->m_PowerUps.erase(std::remove_if(this->m_PowerUps.begin(),
                                          this->m_PowerUps.end(),
                                          [](const PowerUp& powerUp) {
                                              return powerUp.get_Destroyed() &&
                                                     !powerUp.get_Activated();
                                          }),
                           this->m_PowerUps.end());
}

bool ShouldSpawn(unsigned int chance)
{
    unsigned int random = rand() % chance;
    return random == 0;
}

void Game::SpawnPowerUps(GameObject& block)
{
    if (ShouldSpawn(75))
    {
        this->m_PowerUps.push_back(
            PowerUp("speed",
                    glm::vec3(0.5f, 0.5f, 1.0f),
                    0.0f,
                    block.get_Position(),
                    ResourceManager::GetTexture("powerup_speed")));
    }
    if (ShouldSpawn(75))
    {
        this->m_PowerUps.push_back(
            PowerUp("sticky",
                    glm::vec3(1.0f, 0.5f, 1.0f),
                    20.0f,
                    block.get_Position(),
                    ResourceManager::GetTexture("powerup_sticky")));
    }
    if (ShouldSpawn(75))
    {
        this->m_PowerUps.push_back(
            PowerUp("pass-through",
                    glm::vec3(0.5f, 1.0f, 0.5f),
                    10.0f,
                    block.get_Position(),
                    ResourceManager::GetTexture("powerup_passthrough")));
    }
    if (ShouldSpawn(75))
    {
        this->m_PowerUps.push_back(
            PowerUp("pad-size-increase",
                    glm::vec3(1.0f, 0.6f, 0.4f),
                    0.0f,
                    block.get_Position(),
                    ResourceManager::GetTexture("powerup_increase")));
    }
    if (ShouldSpawn(15))
    {
        this->m_PowerUps.push_back(
            PowerUp("confuse",
                    glm::vec3(1.0f, 0.3f, 0.3f),
                    15.0f,
                    block.get_Position(),
                    ResourceManager::GetTexture("powerup_confuse")));
    }
    if (ShouldSpawn(15))
    {
        this->m_PowerUps.push_back(
            PowerUp("chaos",
                    glm::vec3(0.9f, 0.25f, 0.25f),
                    15.0f,
                    block.get_Position(),
                    ResourceManager::GetTexture("powerup_chaos")));
    }
}

bool CheckCollision(const GameObject& one, const GameObject& two)
{
    bool collisionX =
        one.get_Position().x + one.get_Size().x >= two.get_Position().x &&
        two.get_Position().x + two.get_Size().x >= one.get_Position().x;
    bool collisionY =
        one.get_Position().y + one.get_Size().y >= two.get_Position().y &&
        two.get_Position().y + two.get_Size().y >= one.get_Position().y;
    return collisionX && collisionY;
}

Direction VectorDirection(glm::vec2 target)
{
    glm::vec2    compass[]  = { glm::vec2(0.0f, 1.0f),
                            glm::vec2(1.0f, 0.0f),
                            glm::vec2(0.0f, -1.0f),
                            glm::vec2(-1.0f, 0.0f) };
    float        max        = 0.0f;
    unsigned int best_match = -1;
    for (unsigned int i = 0; i < 4; ++i)
    {
        float dot_product = glm::dot(glm::normalize(target), compass[i]);
        if (dot_product > max)
        {
            max        = dot_product;
            best_match = i;
        }
    }
    return Direction(best_match);
}

Collision CheckCollosion(const BallObject& one, const GameObject& two)
{
    glm::vec2 center(one.get_Position() + one.get_Radius());

    glm::vec2 aabb_half_extens(two.get_Size().x / 2.0f,
                               two.get_Size().y / 2.0f);
    glm::vec2 aabb_center(two.get_Position().x + aabb_half_extens.x,
                          two.get_Position().y + aabb_half_extens.y);

    glm::vec2 difference = center - aabb_center;
    glm::vec2 clamped =
        glm::clamp(difference, -aabb_half_extens, aabb_half_extens);

    glm::vec2 closest = aabb_center + clamped;

    difference = closest - center;

    if (glm::length(difference) < one.get_Radius())
    {
        return std::make_tuple(true, VectorDirection(difference), difference);
    }
    else
    {
        return std::make_tuple(false, Direction::UP, glm::vec2(0.0f, 0.0f));
    }
}

bool IsOtherPowerUpActive(std::vector<PowerUp>& powerUps, std::string type)
{
    // Check if another PowerUp of the same type is still active
    // in which case we don't disable its effect (yet)
    for (const PowerUp& powerUp : powerUps)
    {
        if (powerUp.get_Activated())
            if (powerUp.get_Type() == type)
                return true;
    }
    return false;
}

std::array<bool, 1024>& Game::get_Keys()
{
    return m_Keys;
}

bool Game::get_Key(const unsigned int index)
{
    if (index > 0 && index < m_Keys.size())
    {
        return m_Keys[index];
    }
    else
    {
        std::cerr << "GAME::GET_KEY::ERROR: invalid index = " << index
                  << " , when m_Keys.size() = " << m_Keys.size() << std::endl;
        return false;
    }
}

std::vector<GameLevel>& Game::get_Levels()
{
    return m_Levels;
}

GameLevel& Game::get_Level(const unsigned int index)
{
    if (index > 0 && index < m_Keys.size())
    {
        return m_Levels[index];
    }
    else
    {
        std::cerr << "GAME::GET_LEVEL::ERROR: invalid index = " << index
                  << " , when m_Levels.size() = " << m_Levels.size()
                  << std::endl;
        static GameLevel invalidResult;
        return invalidResult;
    }
}

void Game::set_Keys(const std::array<bool, 1024>& newKeys)
{
    m_Keys = newKeys;
}

void Game::set_Key(const unsigned int index, const bool newValue)
{
    if (index > 0 && index < m_Keys.size())
    {
        m_Keys[index] = newValue;
    }
    else
    {
        std::cerr << "GAME::SET_KEY::ERROR: invalid index = " << index
                  << " , when m_Keys.size() = " << m_Keys.size() << std::endl;
    }
}

void Game::set_Levels(const std::vector<GameLevel>& newLevels)
{
    m_Levels = newLevels;
}

void Game::set_Level(const unsigned int index, const GameLevel& newLevel)
{
    if (index > 0 && index < m_Keys.size())
    {
        m_Levels[index] = newLevel;
    }
    else
    {
        std::cerr << "GAME::SET_LEVEL::ERROR: invalid index = " << index
                  << " , when m_Levels.size() = " << m_Levels.size()
                  << std::endl;
    }
}
