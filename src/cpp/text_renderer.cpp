#include "text_renderer.hpp"

#include "glm/gtc/matrix_transform.hpp"

#include "ft2build.h"
#include FT_FREETYPE_H

#include "resource_manager.h"

#include <iostream>

TextRenderer::TextRenderer(unsigned int width, unsigned int height)
{
    this->TextShader =
        ResourceManager::LoadShader("../../src/shaders/text_2d.vs",
                                    "../../src/shaders/text_2d.fs",
                                    nullptr,
                                    "text");
    this->TextShader.SetMatrix4(
        "projection",
        glm::ortho(
            0.0f, static_cast<float>(width), static_cast<float>(height), 0.0f),
        true);
    this->TextShader.SetInteger("text", 0);

    // Загрузка VAO/VBO для текстурных прямоугольников
    glGenVertexArrays(1, &this->VAO);
    glGenBuffers(1, &this->VBO);
    glBindVertexArray(this->VAO);
    glBindBuffer(GL_ARRAY_BUFFER, this->VBO);
    glBufferData(GL_ARRAY_BUFFER, sizeof(float) * 6 * 4, NULL, GL_DYNAMIC_DRAW);
    glEnableVertexAttribArray(0);
    glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, 4 * sizeof(float), 0);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glBindVertexArray(0);
}

void TextRenderer::Load(std::string font, unsigned int fontSize)
{

    // Сначала очищаем ранее загруженные символы
    this->Characters.clear();

    // Затем инициализируем и загружаем библиотеку FreeType
    FT_Library ft;
    if (FT_Init_FreeType(&ft)) // все функции в случае ошибки возвращают
                               // значение, отличное от 0
    {
        std::cout << "ERRPR::FREETYPE: Could not init FreeType Library"
                  << std::endl
                  << std::flush;
    }

    // Загружаем шрифта в качестве face
    FT_Face face;
    if (FT_New_Face(ft, font.c_str(), 0, &face))
    {
        std::cout << "ERROR::FREETYPE: Failed to load font" << std::endl
                  << std::flush;
    }

    // Устанавливаем размер загружаемых глифов
    FT_Set_Pixel_Sizes(face, 0, fontSize);

    // Отключаем ограничение на выравнивание байтов
    glPixelStorei(GL_UNPACK_ALIGNMENT, 1);

    // Предварительно загружаем/компилируем символы шрифта и сохраняем их
    for (GLubyte c = 0; c < 255; ++c)
    {
        // Хагрузка символа глифа
        if (FT_Load_Char(face, c, FT_LOAD_RENDER))
        {
            std::cout << "ERROR::FREETYPE: Failed to load Glyph" << std::endl
                      << std::flush;
            continue;
        }

        // Генерация текстуры
        unsigned int texture;
        glGenTextures(1, &texture);
        glBindTexture(GL_TEXTURE_2D, texture);
        glTexImage2D(GL_TEXTURE_2D,
                     0,
                     GL_RED,
                     face->glyph->bitmap.width,
                     face->glyph->bitmap.rows,
                     0,
                     GL_RED,
                     GL_UNSIGNED_BYTE,
                     face->glyph->bitmap.buffer);

        // Установка параметров текстур
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

        // Теперь сохраняем символы для их дальнейшего использования
        Character character = {
            texture,
            glm::ivec2(face->glyph->bitmap.width, face->glyph->bitmap.rows),
            glm::ivec2(face->glyph->bitmap_left, face->glyph->bitmap_top),
            static_cast<unsigned int>(face->glyph->advance.x)
        };
        Characters.insert(std::pair<char, Character>(c, character));
    }
    glBindTexture(GL_TEXTURE_2D, 0);

    //Когда закончили, освобождаем ресурсы FreeType
    FT_Done_Face(face);
    FT_Done_FreeType(ft);
}

void TextRenderer::RenderText(
    std::string text, float x, float y, float scale, glm::vec3 color)
{
    // Активируем соответсвтующее состание рендера
    this->TextShader.Use();
    this->TextShader.SetVector3f("textColor", color);
    glActiveTexture(GL_TEXTURE0);
    glBindVertexArray(this->VAO);

    //Цикл по всем символам
    std::string::const_iterator c;
    for (c = text.begin(); c != text.end(); ++c)
    {
        Character ch = Characters[*c];

        float xpos = x + ch.Bearing.x * scale;
        float ypos =
            y + (this->Characters['H'].Bearing.y - ch.Bearing.y) * scale;

        float w = ch.Size.x * scale;
        float h = ch.Size.y * scale;

        // Обновляем VBO для каждого символа
        float vertices[6][4] = {
            { xpos, ypos + h, 0.0f, 1.0f },     { xpos + w, ypos, 1.0f, 0.0f },
            { xpos, ypos, 0.0f, 0.0f },         { xpos, ypos + h, 0.0f, 1.0f },
            { xpos + w, ypos + h, 1.0f, 1.0f }, { xpos + w, ypos, 1.0f, 0.0f }
        };

        // Рендерим на прямоугольник текстуру глифа
        glBindTexture(GL_TEXTURE_2D, ch.TextureID);

        // Обновляем содержимое памяти VBO
        glBindBuffer(GL_ARRAY_BUFFER, this->VBO);
        glBufferSubData(GL_ARRAY_BUFFER,
                        0,
                        sizeof(vertices),
                        vertices); // обязательно используйте функцию
                                   // glBufferSubData(), а не glBufferData()
        glBindBuffer(GL_ARRAY_BUFFER, 0);

        // Рендерим прямоугольник
        glDrawArrays(GL_TRIANGLES, 0, 6);

        // Теперь смещаем курсор к следующему глифу
        x += (ch.Advance >> 6) * scale; // битовый сдвиг на 6, чтобы получить
                                        // значение в пикселях (2^6 = 64)
    }
    glBindVertexArray(0);
    glBindTexture(GL_TEXTURE_2D, 0);
}
