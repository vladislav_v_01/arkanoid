#include "game_level.hpp"

#include <fstream>
#include <iostream>
#include <sstream>
#include <string>

GameLevel::GameLevel() {}

void GameLevel::Load(const char*  file,
                     unsigned int levelWidth,
                     unsigned int levelHeight)
{
    this->m_Bricks.clear();

    unsigned int                           tileCode;
    GameLevel                              level;
    std::string                            line;
    std::ifstream                          ifstream(file);
    std::vector<std::vector<unsigned int>> tileData;

    if (ifstream)
    {
        while (std::getline(ifstream, line))
        {
            std::istringstream        sstream(line);
            std::vector<unsigned int> row;
            while (sstream >> tileCode)
            {
                row.push_back(tileCode);
            }
            tileData.push_back(row);
        }
        if (tileData.size() > 0)
        {
            this->init(tileData, levelWidth, levelHeight);
        }
    }
    else
    {
        std::cerr << "GAME_LEVEL::LOAD::ERROR: invalid file name "
                  << std::string(file) << std::endl;
    }
}

void GameLevel::Draw(SpriteRenderer& renderer)
{
    for (GameObject& tile : this->m_Bricks)
    {
        if (!tile.get_Destroyed())
        {
            tile.Draw(renderer);
        }
    }
}

bool GameLevel::IsCompleted()
{
    for (GameObject& tile : this->m_Bricks)
    {
        if (!tile.get_IsSolid() && !tile.get_Destroyed())
        {
            return false;
        }
    }
    return true;
}

void GameLevel::set_Brick(const GameObject& brick, const unsigned int index)
{
    if (index > 0 && index < m_Bricks.size())
    {
        m_Bricks[index] = brick;
    }
    else
    {
        std::cerr << "GAME_LEVEL::GET_BRICK::ERROR: invalid index = " << index
                  << " , when m_keys.size() = " << m_Bricks.size() << std::endl;
    }
}

void GameLevel::set_Bricks(const std::vector<GameObject>& bricks)
{
    m_Bricks = bricks;
}

GameObject& GameLevel::get_Brick(const unsigned int index)
{
    if (index > 0 && index < m_Bricks.size())
    {
        return m_Bricks[index];
    }
    else
    {
        std::cerr << "GAME_LEVEL::GET_BRICK::ERROR: invalid index = " << index
                  << " , when m_keys.size() = " << m_Bricks.size() << std::endl;
        static GameObject invalidResult;
        return invalidResult;
    }
}

std::vector<GameObject>& GameLevel::get_Bricks()
{
    return m_Bricks;
}

void GameLevel::init(std::vector<std::vector<unsigned int>> tileData,
                     unsigned int                           levelWidth,
                     unsigned int                           levelHeight)
{
    unsigned int height      = tileData.size();
    unsigned int width       = tileData[0].size();
    float        unit_width  = levelWidth / static_cast<float>(width);
    float        unit_height = levelHeight / static_cast<float>(height);

    for (unsigned int y = 0; y < height; ++y)
    {
        for (unsigned int x = 0; x < width; ++x)
        {
            if (tileData[y][x] == 1) // solid
            {
                glm::vec2  pos(unit_width * x, unit_height * y);
                glm::vec2  size(unit_width, unit_height);
                GameObject obj(pos,
                               size,
                               ResourceManager::GetTexture("block_solid"),
                               glm::vec3(0.8f, 0.8f, 0.7f));
                obj.set_IsSolid(true);
                this->m_Bricks.push_back(obj);
            }
            else if (tileData[y][x] > 1)
            {
                glm::vec3 color = glm::vec3(1.0f); // white
                if (tileData[y][x] == 2)
                {
                    color = glm::vec3(0.2f, 0.6f, 1.0f);
                }
                else if (tileData[y][x] == 3)
                {
                    color = glm::vec3(0.0f, 0.7f, 0.0f);
                }
                else if (tileData[y][x] == 4)
                {
                    color = glm::vec3(0.8f, 0.8f, 0.4f);
                }
                else if (tileData[y][x] == 5)
                {
                    color = glm::vec3(1.0f, 0.5f, 0.0f);
                }
                glm::vec2 pos(unit_width * x, unit_height * y);
                glm::vec2 size(unit_width, unit_height);
                this->m_Bricks.push_back(GameObject(
                    pos, size, ResourceManager::GetTexture("block"), color));
            }
        }
    }
}
