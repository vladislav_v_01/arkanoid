#ifndef GAME_HPP
#define GAME_HPP

#include "glad.h"

#include "GLFW/glfw3.h"

#include "property.h"

#include "game_level.hpp"
#include "power_up.hpp"

#include <array>
#include <vector>

enum class GameState
{
    GAME_ACTIVE,
    GAME_MENU,
    GAME_WIN
};

enum class Direction
{
    UP,
    RIGHT,
    DOWN,
    LEFT
};

using Collision = std::tuple<bool, Direction, glm::vec2>;

const glm::vec2 PLAYER_SIZE(100.0f, 20.0f);
const float     PLAYER_VELOCITY(500.0f);

const glm::vec2 INITIAL_BALL_VELOCITY(100.0f, -350.0f);
const float     BALL_RADIUS = 12.5f;

class Game
{
public:
    Game(unsigned int width, unsigned int height);
    ~Game();

    void Init();

    void ProcessInput(float dt);
    void Update(float dt);
    void Render();

    void DoCollision();

    void ResetLevel();
    void ResetPlayer();

    void SpawnPowerUps(GameObject& block);
    void UpdatePowerUps(float dt);

    std::array<bool, 1024>& get_Keys();
    bool                    get_Key(const unsigned int index);
    std::vector<GameLevel>& get_Levels();
    GameLevel&              get_Level(const unsigned int index);

    void set_Keys(const std::array<bool, 1024>& newKeys);
    void set_Key(const unsigned int index, const bool newValue);
    void set_Levels(const std::vector<GameLevel>& newLevel);
    void set_Level(const unsigned int index, const GameLevel& newLevel);

    bool m_KeysProcessed[1024];

private:
    PROPERTY(GameState, State);
    PROPERTY(unsigned int, Width);
    PROPERTY(unsigned int, Height);
    PROPERTY(unsigned int, Level);
    PROPERTY(unsigned int, Lives);
    PROPERTY(std::vector<PowerUp>, PowerUps);

    std::array<bool, 1024> m_Keys;
    std::vector<GameLevel> m_Levels;
};

#endif
