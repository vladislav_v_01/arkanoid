#ifndef POWERUP_H
#define POWERUP_H

#include "glad.h"

#include "glm/glm.hpp"

#include <string>

#include "gameobject.hpp"
#include "property.h"

const glm::vec2 POWERUP_SIZE(60.0f, 20.0f);

const glm::vec2 VELOCITY(0.0f, 150.0f);

class PowerUp : public GameObject
{
public:
    PowerUp(std::string type,
            glm::vec3   color,
            float       duration,
            glm::vec2   position,
            Texture2D   texture)
        : GameObject(position, POWERUP_SIZE, texture, color, VELOCITY)
        , m_Type(type)
        , m_Duration(duration)
        , m_Activated()
    {
    }

    friend bool operator==(const PowerUp& pu1, const PowerUp& pu2);

private:
    PROPERTY(std::string, Type);
    PROPERTY(float, Duration);
    PROPERTY(bool, Activated);
};

#endif // POWERUP_H
