#ifndef SPRITERENDERER_HPP
#define SPRITERENDERER_HPP

#include "glad.h"

#include "glm/glm.hpp"
#include "glm/gtc/matrix_transform.hpp"

#include "shader.h"
#include "texture.h"

class SpriteRenderer
{
public:
    SpriteRenderer(const Shader& shader);

    ~SpriteRenderer();

    void DrawSprite(const Texture2D& texture,
                    glm::vec2        position,
                    glm::vec2        size   = glm::vec2(10.0f, 10.0f),
                    float            rotate = 0.0f,
                    glm::vec3        color  = glm::vec3(1.0));

private:
    Shader       m_shader;
    unsigned int m_quadVAO;

    void initRenderData();
};

#endif // SPRITERENDER_HPP
