#ifndef PARTICLEGENERATOR_H
#define PARTICLEGENERATOR_H

#include "glad.h"

#include "glm/glm.hpp"

#include "gameobject.hpp"
#include "shader.h"
#include "texture.h"

#include <vector>

struct Particle
{
    glm::vec2 Position, Velocity;
    glm::vec4 Color;
    float     Life;

    Particle()
        : Position(0.0f)
        , Velocity(1.0f)
        , Life(0.0f)
    {
    }
};

class ParticleGenerator
{
public:
    ParticleGenerator(Shader shader, Texture2D texture, unsigned int amount);

    void Update(float        dt,
                GameObject&  object,
                unsigned int newParticles,
                glm::vec2    offset = glm::vec2(0.0f, 0.0f));

    void Draw();

private:
    std::vector<Particle> m_Particles;
    unsigned int          m_Amount;
    Shader                m_Shader;
    Texture2D             m_Texture;
    unsigned int          VAO;

    void init();

    unsigned int firstUnusedParticle();

    void respawnParticle(Particle&   particle,
                         GameObject& object,
                         glm::vec2   offset = glm::vec2(0.0f, 0.0f));
};

#endif // PARTICLEGENERATOR_H
