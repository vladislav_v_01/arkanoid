#ifndef TEXTRENDERER_HPP
#define TEXTRENDERER_HPP

#include <map>

#include "glad.h"

#include "glm/glm.hpp"

#include "shader.h"
#include "texture.h"

// Информация о состоянии символа, загруженного с помощью библиотеки FreeType
struct Character
{
    unsigned int TextureID; // ID текстуры глифа
    glm::ivec2   Size;      // размер глифа
    glm::ivec2
        Bearing; // смещение от линии шрифта до верхнего левого угла глифа
    unsigned int
        Advance; // горизонтальное смещение для перехода к следующему глифу
};

// Класс TextRenderer предназначен для рендеринга текста, отображаемого шрифтом,
// загруженным с помощью библиотеки FreeType. Загруженный шрифт обрабатывается и
// сохраняется для последующего рендеринга в виде списка символов
class TextRenderer
{
public:
    // список предварительно скомпилированных символов
    std::map<char, Character> Characters;

    // Шейдер, используемый для рендеринга текста
    Shader TextShader;

    // Конструктор
    TextRenderer(unsigned int width, unsigned int height);

    // Список предварительно скопилированных символов из заданного шрифта
    void Load(std::string font, unsigned int fontSize);

    // Рендеринг строки текста с использованием предварительно скомпилированного
    // списка символов
    void RenderText(std::string text,
                    float       x,
                    float       y,
                    float       scale,
                    glm::vec3   color = glm::vec3(1.0f));

private:
    // Состояние рендеринга
    unsigned int VAO, VBO;
};

#endif // TEXTRENDERER_HPP
