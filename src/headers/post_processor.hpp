#ifndef POSTPROCESSOR_HPP
#define POSTPROCESSOR_HPP

#include "glad.h"

#include "glm/glm.hpp"

#include "property.h"
#include "shader.h"
#include "sprite_renderer.hpp"
#include "texture.h"

class PostProcessor
{
public:
    PostProcessor(Shader shader, unsigned int width, unsigned int height);

    void BeginRender();

    void EndRender();

    void Render(float time);

private:
    void initRenderData();

private:
    PROPERTY(Shader, PostProcessingShader);
    PROPERTY(Texture2D, Texture);
    PROPERTY(unsigned int, Width);
    PROPERTY(unsigned int, Height);
    PROPERTY(bool, Confuse);
    PROPERTY(bool, Chaos);
    PROPERTY(bool, Shake);
    PROPERTY(unsigned int, MSFBO);
    PROPERTY(unsigned int, FBO);
    PROPERTY(unsigned int, RBO);
    PROPERTY(unsigned int, VAO);
};

#endif // POSTPROCESSOR_HPP
