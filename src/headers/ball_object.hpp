#ifndef BALLOBJECT_HPP
#define BALLOBJECT_HPP

#include "gameobject.hpp"
#include "property.h"
#include "texture.h"

class BallObject : public GameObject
{
public:
    BallObject();
    BallObject(glm::vec2 pos,
               float     radius,
               glm::vec2 velocity,
               Texture2D sprite);

    glm::vec2 Move(float dt, unsigned int window_width);
    void      Reset(glm::vec2 position, glm::vec2 velocity);

private:
    PROPERTY(float, Radius);
    PROPERTY(bool, Stuck);
    PROPERTY(bool, Sticky);
    PROPERTY(bool, PassThrough);
};

#endif // BALLOBJECT_H
