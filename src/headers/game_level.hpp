#ifndef GAMELEVEL_HPP
#define GAMELEVEL_HPP

#include "glad.h"

#include "glm/glm.hpp"

#include "gameobject.hpp"
#include "resource_manager.h"
#include "sprite_renderer.hpp"

#include <vector>

class GameLevel
{
public:
    GameLevel();

    void Load(const char*  file,
              unsigned int levelWidth,
              unsigned int levelHeight);
    void Draw(SpriteRenderer& renderer);

    bool IsCompleted();

    void set_Brick(const GameObject& brick, const unsigned int index);
    void set_Bricks(const std::vector<GameObject>& bricks);

    GameObject&              get_Brick(const unsigned int index);
    std::vector<GameObject>& get_Bricks();

private:
    void init(std::vector<std::vector<unsigned int>> tileData,
              unsigned int                           levelWidth,
              unsigned int                           levelHeight);

private:
    std::vector<GameObject> m_Bricks;
};

#endif // GAMELEVEL_H
