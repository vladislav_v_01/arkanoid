#ifndef GAMEOBJECT_HPP
#define GAMEOBJECT_HPP

#include "glad.h"

#include "glm/glm.hpp"

#include "property.h"
#include "sprite_renderer.hpp"
#include "texture.h"

class GameObject
{
public:
    GameObject();
    GameObject(glm::vec2 pos,
               glm::vec2 size,
               Texture2D sprite,
               glm::vec3 color    = glm::vec3(1.0f),
               glm::vec2 velocity = glm::vec2(0.0f, 0.0f));

    virtual void Draw(SpriteRenderer& renderer);

private:
    PROPERTY(glm::vec2, Position);
    PROPERTY(glm::vec2, Size);
    PROPERTY(glm::vec2, Velocity);
    PROPERTY(glm::vec3, Color);
    PROPERTY(float, Rotation);
    PROPERTY(bool, IsSolid);
    PROPERTY(bool, Destroyed);
    PROPERTY(Texture2D, Sprite);
};

#endif // GAMEOBJECT_H
